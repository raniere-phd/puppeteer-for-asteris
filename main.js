'use strict';

const path = require('path');
const fs = require('fs');

const prompt = require('prompt-sync')();

const puppeteer = require('puppeteer-core');

const download_root = path.resolve('/media/raniere/Jockey Club/', 'Downloads');
const download_tmp = path.resolve(download_root, 'tmp');
const clean_download_path = path.resolve(download_root, 'clean');
const mark_download_path = path.resolve(download_root, 'mark');

const horses = require('./horses.json');  /* synchronous function */

const crawler = async function(web_browser_end_point) {
    const MAX_RUN = 100;

    var browser = await puppeteer.connect({
        browserWSEndpoint: web_browser_end_point,
    });

    console.log(`Successful connection with ${web_browser_end_point}.`);

    var page = await browser.newPage();
    page.defaultNavigationTimeout = 2 * 60 * 1000;
    page.defaultTimeout = 2 * 60 * 1000;

    var run_count = 0;
    var incomplete = true;
    do {
	try {
	    run_count += 1;
	    console.log('Run: ' + run_count + ' of ' + MAX_RUN);
	    
	    await one_run(page);
	    incomplete = false;
	}
	catch (error) {
	    console.log(error);
	    console.log('Restarting download from the begin!');
	    incomplete = true;
	}
    } while(incomplete);
};

const one_run = async function(page) {
    await page.goto(
	'https://keystone.asteris.com/#/studies?filter-preset=2448c77a-0100-8528-0419-ffffff210812',
	{
            waitUntil: 'networkidle0'
        }
    );

    var horse_count = 0;
    var page_count = 0;
    var has_next = true;

    while(has_next) {
	page_count += 1;
	console.log('Processing page #' + page_count);

	var table = await page.$('tbody');

	var rows = await table.$$('tr');

	for (var row of rows) {
	    var row_info = await row.$$('td');
	    var row_id = await row_info[2].evaluate(
		el => el.innerText
	    )
	    console.log('ID: ' + row_id);

	    var can_download = false;
	    var new_id;
	    var download_path;
	    for (var horse of horses) {
		if (horse.KeystoneID == row_id) {
		    new_id = horse.Filename;
		    can_download = true;

		    if (horse.WaterMark) {
			download_path = path.resolve(
			    mark_download_path,
			    new_id + '.zip'
			);
		    }
		    else {
			download_path = path.resolve(
			    clean_download_path,
			    new_id + '.zip'
			);
		    }

		    break;
		}
	    }
	    if (!can_download) {
		console.log('Permission not found!');
		console.log('Skipping ' + row_id);
		continue;
	    }
            if (fs.existsSync(download_path)) {
		console.log('File ' + download_path + ' already exists!');
		console.log('Skipping ' + row_id);
		continue;
            }

	    horse_count += 1;
	    console.log('Processing horse #' + horse_count);

	    var downloaded = false;
	    while (!downloaded){
		await Promise.all([
		    row.click(),
		    page.waitForTimeout(10000),
		]);

		var panel = await page.$('tr.expanded');
		// console.log(panel);

		var buttons = await panel.$$('button');
		// console.log(buttons.length);
		// console.log(buttons);
		await Promise.all([
		    buttons[7].click(),
		    page.waitForTimeout(10000),
		]);

		var id_div = await page.$('div.patient-id');
		var id_input = await id_div.$('input');
		await id_input.focus();
		await page.keyboard.type(new_id);

		var footbar = await page.$('div.footer');
		var footbar_buttons = await footbar.$('button');
		
		var download_request_datetime = new Date();
		console.log('Saving file at ' + download_tmp);
		await page._client.send(
		    'Page.setDownloadBehavior',
		    {
			behavior: 'allow',
			downloadPath: download_tmp
		    }
		);
		console.log('Requesting download at ' + download_request_datetime);
		footbar_buttons.click()
		await page.waitForTimeout(5 * 60 * 1000); /* Download might take some time to start */

		var is_downloading;
		var tmp_filename;
		do {
		    is_downloading = false;

		    var all_files = fs.readdirSync(download_tmp);

		    if (all_files.length == 0) {
			/* Download did NOT start or fail */
			downloaded = false;
			tmp_filename = '';
		    }
		    else {
			for (var file of all_files) {
			    if (file.endsWith('crdownload')) {
				is_downloading = true;
				await page.waitForTimeout(1 * 60 * 1000);
			    }
			    else {
				downloaded = true;
				tmp_filename = file;
			    }
			}
		    }
		} while (is_downloading);
		
		var download_end_datetime = new Date();
		console.log('Download ended at ' + download_end_datetime);

		if (downloaded) {
		    try {
			var tmp_path = path.resolve(
			    download_tmp,
			    tmp_filename
			);
			console.log('Renaming ' + tmp_path);
			fs.renameSync(
			    tmp_path,
			    download_path
			);
			console.log('File stored as ' + download_path);

			var download_timeout_datetime = new Date();
			console.log('Moving to next row at ' + download_timeout_datetime);
		    }
		    catch (error) {
			console.log('Rename file failed due:');
			console.log(error);
			console.log('Trying download file again!!!');
			downloaded = false;
		    }
		}
		else {
		    console.log('Download failed due network error.');
		    console.log('Trying again!!!');
		}
	    }

	}

	var nav = await page.$('div.page-buttons');
	var nav_buttons = await nav.$$('button');
	has_next = await nav_buttons[1].evaluate(
	    el => !el.disabled
	)
	if(has_next) {
	    await Promise.all([
		nav_buttons[1].click(),
		page.waitForTimeout(10000),
	    ]);
	}
	else {
	    console.log('Next page is disabled.');
	    console.log('Total number of pages: ' + page_count);
	    console.log('Total number of horses: ' + horse_count);
	}

    }

    /* Can't close the browser */
    // console.log(`Closing connection with ${web_browser_end_point}.`);
    // await browser.close();
    // console.log(`Connection with ${web_browser_end_point} closed!`);

    process.exit(0);
}

var web_browser_end_point;

web_browser_end_point = process.env.WEB_BROWSER_END_POINT;

if (typeof web_browser_end_point == 'undefined' || web_browser_end_point.length == 0) {
    web_browser_end_point = prompt('Web browser end point: ');
    console.log(`web_browser_end_point: ${web_browser_end_point}`);
}

if (web_browser_end_point.length == 0) {
    console.log("Aborting. Please provide a remote browser!");
    process.exit(1);
}

crawler(web_browser_end_point);

