# Puppeteer For Asteris

Script using Puppeteer to download radiograph sets from https://keystone.asteris.com.

## Prerequisites

- [Node.js](https://nodejs.org)
- [npm](https://npmjs.com)
- [Chromium](https://www.chromium.org) or Google Chrome

## Setup

Launch Chromium with remote debugging enabled:

```
$ chromium  --remote-debugging-port=9222 --no-first-run --no-default-browser-check

DevTools listening on ws://127.0.0.1:9222/devtools/browser/2ae369c0-1c2a-4f4b-89ea-34687ab9e3c6
```

Create an enviroment variable to store the address of the web browser:

```
$ export WEB_BROWSER_END_POINT=ws://127.0.0.1:9222/devtools/browser/2ae369c0-1c2a-4f4b-89ea-34687ab9e3c6
```

## Run

```
$ npm start
```

The Chromium tab being used by Puppeteer **must** be in focus (selected as the working tab)
otherwise the script will stop.

## References

- [Connecting Puppeteer to Existing Chrome Window w/ reCAPTCHA](https://medium.com/@jaredpotter1/connecting-puppeteer-to-existing-chrome-window-8a10828149e0) by [Jared Potter](https://medium.com/@jaredpotter1)
- [How to download a file with Puppeteer?](https://www.scrapingbee.com/blog/download-file-puppeteer/) by Kevin Sahin